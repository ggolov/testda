import gulp from 'gulp';
import rename from 'gulp-rename';
import uglify from 'gulp-uglify';
import webpack from 'webpack';
import webpackStream from 'webpack-stream';
import { VueLoaderPlugin } from 'vue-loader';
import { deleteAsync } from 'del';

import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);
import cleanCss from 'gulp-clean-css';
import autoprefixer from 'gulp-autoprefixer';

const isDev = function(){
  return process.env.NODE_ENV === 'development';
}
const pathSrc = [`./src/js/*.js`];
const pathScsc = [`./src/scss/*.scss`, '!./src/scss/constants.scss'];

export function build() 
{
    return gulp.src(pathSrc)
    .pipe(webpackStream({
        mode: isDev() ? 'development' : 'production', 
        module: {
            rules: [
                {
                    test: /\.vue$/,
                    loader: 'vue-loader'
                },
                {
                    test: /\.css$/,
                    use: [
                        'vue-style-loader',
                        'css-loader'
                    ]
                },
            ]
        },
        plugins: [
            new VueLoaderPlugin(),
            
            new webpack.DefinePlugin({
                __VUE_OPTIONS_API__: true,
                __VUE_PROD_DEVTOOLS__: true
            })
        ]
    }))
    .pipe(uglify())
    .pipe(rename({
      suffix: '.min',
    }))
    .pipe(gulp.dest('./dist/js/'));
}

export function styles() {
  return gulp.src(pathScsc)
    .pipe(sass({includePaths: [pathScsc]}).on('error', sass.logError))
    .pipe(autoprefixer({
      cascade: false
    }))
    .pipe(cleanCss({
      level: 2
    }))
    .pipe(rename({
      suffix: '.min',
    }))
    .pipe(gulp.dest('./dist/css/'));
}

export function clean() {
  return deleteAsync([
    './dist/js/*.*'
  ]);
}

export default gulp.series(
  clean,
  styles,
  build
);
