<?php
define("B_PROLOG_INCLUDED", true);

require_once "data/data.php";

$title = "Облачный кондиционер Daichi Alpha A20AVQR3_1Y/A20FVR3_1Y";

?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="initial-scale=1.0, width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="robots" content="noindex, nofollow" />
<title>Тестовое задание для вакансии Frontend Developer</title><?

$arStyles = ["fonts", "css/base.min", "styles"];
foreach ($arStyles as $style) {
  ?><link rel='stylesheet' href='/dist/<?=$style;?>.css?<?=time();?>' type='text/css' /><?
}

?></head>
<body>
  <div class="section">
    <div class="container">
      <div class="item-card__wrapper">
        <div class="item-slider__wrapper" id="slider"></div>
        <div class="item-title__wrapper">
          <h1>Облачный кондиционер Daichi Alpha A20AVQR3_1Y/A20FVR3_1Y + 1 год подписки</h1>
          <div class="item-favorite__wrapper" id="favorite"></div>
        </div>
        <div class="item-subtitle__wrapper">
          <div class="item-props__wrapper">
          <h5>Характеристики</h5>
          <div class="props__table"><?
          foreach ($arProps as $prop) {
            ?><div class="props__row">
              <div class="props__label color-grey"><?=$prop["label"];?></div>
              <div class="props__dots"></div>
              <div class="props__value"><?=$prop["value"];?></div>
            </div><?
          }
          ?></div>
          </div>
          <div class="item-buttons__wrapper">
            <div class="item-price__wrapper box-rounded box-shadow">13 572 ₽</div>
            <div id="cheaper"></div>
          </div>
        </div>
        <div class="item-accordeon__wrapper" id="accordeon"></div>
      </div>
    </div>
  </div>
<script>
<?
$arVars = ["arImages", "arAccordeon"];
foreach ($arVars as $var) {
  if (!empty($$var)) {
?>
var <?=$var;?> = '<?=json_encode($$var, JSON_UNESCAPED_UNICODE);?>';
<?
  }
}
?>
var title = '<?=$title;?>';
</script>
<script src="/dist/js/main.min.js"></script>
</body>
</html>