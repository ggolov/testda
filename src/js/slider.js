import { createApp } from 'vue';
import Slider from './components/Slider.vue';

const slider = createApp(Slider);

document.addEventListener('DOMContentLoaded', () =>
{
  slider.mount('#slider');
});