import { createApp } from 'vue';
import mitt from 'mitt';
import Cheaper from './components/Cheaper.vue';

const emitter = mitt();
const cheaper = createApp(Cheaper);

document.addEventListener('DOMContentLoaded', () =>
{
  cheaper.config.globalProperties.emitter = emitter;
  cheaper.mount('#cheaper');
});