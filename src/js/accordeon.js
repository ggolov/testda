import { createApp } from 'vue';
import Accordeon from './components/Accordeon.vue';

const accordeon = createApp(Accordeon);

document.addEventListener('DOMContentLoaded', () =>
{
  accordeon.mount('#accordeon');
});