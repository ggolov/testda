import { createApp } from 'vue';
import Favorite from './components/Favorite.vue';

const favorite = createApp(Favorite);

document.addEventListener('DOMContentLoaded', () =>
{
  favorite.mount('#favorite');
});